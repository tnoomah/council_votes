

#Load Packages ------------------------------------------------------------------------
#Add all required lists to the list.of.packages object
#missing packages will be automatically installed
list.of.packages <- c("httr",
                      "tidyverse", 
                      "here", 
                      "parsedate", 
                      "stringdist", 
                      "feather")

new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)

#load all required packages
lapply(list.of.packages, require, character.only = TRUE)


#Remove scientific notation
options(scipen=999)

#print working directory
here()




####Bring in functions -----------------------------------------------------------------------------
range_0_to_1 <- function(x, ...){round((x - min(x, ..., na.rm = T)) / (max(x, ..., na.rm = T) - min(x, ..., na.rm = T)),3)*1}





####Bring in the data-------------------------------------------------------------------------------

# vb_df_csv <- read.csv(here("tasks","import","output", "All_Divided_Votes_2019_Session.csv"),
#                   encoding ='latin1',
#                   stringsAsFactors = F)

vb_df <- as.data.frame(read_feather(here("tasks","import","output", "All_Divided_Votes_2019_Session.feather")))




####Clean the df-----------------------------------------------------------------------------------

# basic cleaning of alderman names
vb_df$Alderman <- str_squish(vb_df$Alderman)
vb_df$Alderman <- toupper(vb_df$Alderman)
vb_df$Alderman <- ifelse(vb_df$Alderman == "", "VACANT", vb_df$Alderman)



# NOTE: created this to deal with alderman name variations from 12/2012 on
match_alderman_names <- function(alderman_name, all_names) {
  
  
  all_names_minus_alderman_name <- all_names[!(all_names %in% alderman_name)]
  
   temp_df <- data.frame(alderman_name, 
                         all_names_minus_alderman_name,
                 lv = stringdist(alderman_name, all_names_minus_alderman_name, method='lv', q=2),
                 jaccard = stringdist(alderman_name, all_names_minus_alderman_name, method='jaccard', q=2),
                 cosine = stringdist(alderman_name, all_names_minus_alderman_name, method='cosine', q=2),
                 jw = stringdist(alderman_name, all_names_minus_alderman_name, method='jw', q=2),
                 qgram = stringdist(alderman_name, all_names_minus_alderman_name, method='qgram', q=2),
                 stringsAsFactors = F)
   the_cols <- c("lv","qgram")
 
   #temp_df[is.nan(as.numeric(temp_df))] <- NA
   temp_df[the_cols] <- sapply(temp_df[the_cols], range_0_to_1)
   temp_df$sum_normalized_score <- rowSums(temp_df[,3:7], na.rm = T)
   
   
   temp_df <- temp_df %>%
     mutate(similarity_rank = rank(sum_normalized_score, ties.method = "first")) %>%
     arrange(similarity_rank) #>%>
    #filter(similarity_rank <= 5)  
    
   
   temp_df
  
}
  

alderman_similarity_score <- vector("character")
for(alderman in unique(na.omit(vb_df$Alderman))) {
  
  the_df <- match_alderman_names(alderman_name = alderman, all_names = unique(na.omit(vb_df$Alderman)))
  
  alderman_similarity_score <- bind_rows(alderman_similarity_score, the_df)
  
  
}

alderman_similarity_score <- alderman_similarity_score[-1,]
alderman_similarity_score$sum_jac_cos_jw <- rowSums(alderman_similarity_score[,c("jaccard","cosine","jw")], na.rm = T)
alderman_similarity_score <- alderman_similarity_score %>% arrange(sum_normalized_score)

#Review to find the same Alderman that appears with spelling variations
#View(alderman_similarity_score %>% arrange(sum_normalized_score))

# Based on the review above, created this file by hand in excel
ald_names_fix_df <- read.csv(here("tasks","import","hand","CCC_Divided_Roll_Calls_Ald_Names_Clean.csv"), 
                             encoding ='latin1',
                             stringsAsFactors=FALSE)

# Clean spelling variations for unique Alderman
vb_df$Alderman_Clean <- ald_names_fix_df[match(vb_df$Alderman, ald_names_fix_df$alderman_name_OLD),"alderman_name_NEW"]
vb_df$Alderman_Clean <- ifelse(is.na(vb_df$Alderman_Clean), vb_df$Alderman, vb_df$Alderman_Clean)

# Because there are multiple MOORE's, must fix conditionally
vb_df$Alderman_Clean <- ifelse(vb_df$Alderman_Clean == "MOORE" & vb_df$Ward == 49, "J. MOORE", 
                               ifelse(vb_df$Alderman_Clean == "MOORE" & vb_df$Ward == 17, "D. MOORE", vb_df$Alderman_Clean))

# J. Moore is left office as of May 2019, so we only need Moore after that
vb_df$Alderman_Clean <- ifelse(vb_df$Alderman_Clean == "D. MOORE" & vb_df$Date_from_clerk_website >= "2019-05-29", "MOORE", vb_df$Alderman_Clean)

#Redo similarity score to see if anything missed
alderman_similarity_score <- vector("character")
for(alderman in unique(na.omit(vb_df$Alderman_Clean))) {
  
  the_df <- match_alderman_names(alderman_name = alderman, all_names = unique(na.omit(vb_df$Alderman_Clean)))
  
  alderman_similarity_score <- bind_rows(alderman_similarity_score, the_df)
  
  
}

alderman_similarity_score <- alderman_similarity_score[-1,]
alderman_similarity_score$sum_jac_cos_jw <- rowSums(alderman_similarity_score[,c("jaccard","cosine","jw")], na.rm = T)
alderman_similarity_score <- alderman_similarity_score %>% arrange(sum_normalized_score)

# Review to identify any more spelling variations for the same alderman
#View(table(vb_df$Alderman_Clean))

#Change names so code below works
vb_df$Alderman_Raw <- vb_df$Alderman
vb_df$Alderman <- vb_df$Alderman_Clean
vb_df <- vb_df %>% select(-Alderman_Clean)

####Transform the df for analysis, add features ----------------------------------------------------

#Change vote labels 
##Only "Yes" or "No" votes count towards an Alderperson's final 'agreement' score -- so, 
##Dick and Marco do not include Absent, Not Voting, Vacant and Recused values in their analysis. 

table(vb_df$Vote)
#Key:
# 1 –Yes
# 0 –No
# 2 –Not Voting
# 3 –Absent
# 4 –Excused from Voting / Recused 
# 5 -Vacancy

vb_df$Votes_Cleaned <- recode(vb_df$Vote,
                              A = 3,
                              N = 0,
                              NV = 2,
                              R = 4,
                              Recused = 4,
                              V = 5,
                              Y = 1,
                              .default = NA_real_)


# Add vote totals (some are missing from the scrape)
vb_df <- vb_df %>%
  group_by(Vote_ID) %>%
  mutate(Votes_Total_Yes = sum(Votes_Cleaned == 1, na.rm = T),
         Votes_Total_No = sum(Votes_Cleaned == 0, na.rm = T)) %>%
  mutate(Votes_Total = Votes_Total_Yes + Votes_Total_No) %>%
  ungroup()

 
# Add vote decision
vb_df <- vb_df %>%
  mutate(VOTE_DECISION = if_else(Votes_Total > 1 & Votes_Total == Votes_Total_Yes, "Carried Unanimously", 
                                 if_else(Votes_Total_Yes > Votes_Total_No, "Carried",
                                         if_else(Votes_Total_Yes < Votes_Total_No, "Lost",
                                                 if_else(Votes_Total > 1 & Votes_Total_Yes == Votes_Total_No, "Tie",NA_character_)))))

# Add date features
vb_df$Weekday <- weekdays(vb_df$Date_from_clerk_website) #add day of week
vb_df$Month <- format(vb_df$Date_from_clerk_website,"%B")
vb_df$Day <- format(vb_df$Date_from_clerk_website, "%d")
vb_df$Year <- format(vb_df$Date_from_clerk_website, "%Y")

# Convert back to dataframe
vb_df <- as.data.frame(vb_df)

####Create df for sharing and shiny ---------------------------------------------------------------

councilmatic_prefix <- "https://chicago.councilmatic.org/legislation/"

shiny_df <- as.data.frame(vb_df %>%
  select(Record.No., Title, 
         Date_from_clerk_website, Year,
         Alderman, Ward, Vote, Votes_Total_Yes, Votes_Total_No, VOTE_DECISION) %>%
  rename(BILL_ID = Record.No.,
         BILL_TITLE = Title,
         VOTE_DATE = Date_from_clerk_website,
         VOTE_YEAR = Year,
         YES_VOTES = Votes_Total_Yes,
         NO_VOTES = Votes_Total_No,
         ALDERMAN_NAME = Alderman,
         ALDERMAN_WARD = Ward,
         ALDERMAN_VOTE = Vote) %>%
  filter(!is.na(ALDERMAN_WARD)) %>%
  select(BILL_ID:VOTE_YEAR,YES_VOTES, NO_VOTES,
         VOTE_DECISION, everything()) %>%
  arrange(BILL_ID, VOTE_DATE) %>%
  # Link the bill names to Chicago Councilmatic bill pages
  mutate(BILL_ID_URL = paste0("<a href='",
                                councilmatic_prefix, tolower(BILL_ID),
                                "' target='_blank'>",
                                BILL_ID,
                                "</a>")))
  
 
# Confirm that all BILL_ID_URLs point to a working URL
all_bill_urls <- paste0(councilmatic_prefix, tolower(unique(na.omit(shiny_df$BILL_ID))))

out_file <- vector("character")
for(i in all_bill_urls){
  #print(i)
  the_start_time <- Sys.time()
  z <- GET(i)
  output <- http_status(z)
  is_there_an_error <- http_error(z)
  path_name <- parse_url(i)$path
  out_file <- rbind(out_file, data.frame(website = i,
                                         path = path_name, 
                                         category = output$category, 
                                         reason = output$reason, 
                                         message = output$message, 
                                         is_there_an_error,
                                         time_elapsed_seconds = round(as.numeric(Sys.time() - the_start_time),2), stringsAsFactors = F))
}

num_errors = sum(out_file$is_there_an_error, na.rm = T)

ifelse(num_errors == 0, "SUCCESS: All BILL_ID urls work", warning("STOP: Review the urls that do not work and fix"))

if (num_errors > 0) {
  
  View(out_file %>% 
         filter(is_there_an_error == TRUE))
  
}


####Summary Stats on Alderman ----------------------------------------------------------------------


alderman_stats <- vb_df %>%
  filter(!is.na(Alderman)) %>%
  group_by(Alderman, Year, Ward) %>%
  summarise(n_rows = n(),
            min_date = min(Date_from_clerk_website),
            max_date = max(Date_from_clerk_website)) %>% #Wards = paste(unique(na.omit(Ward)), collapse = "||")
  ungroup() %>%
  group_by(Alderman, Ward) %>%
  summarise(min_year = min(Year), 
            max_year = max(Year),
            min_date = min(min_date),
            max_date = max(max_date),
            total_rows = sum(n_rows))  #Wards = paste(unique(na.omit(Ward)), collapse = "||")



####Create bill table -- rows are bills with divided roll calls and key variables are aldernan------
#Spread DF so that Record number becomes observational unit, and alderman become variables

bill_table <- vb_df %>% 
  #filter(Date_from_clerk_website >= "2019-05-28") %>%
  filter(!is.na(Alderman), !is.na(Votes_Cleaned)) %>%
  #filter(Votes_Cleaned <= 1) %>%
  select(Record.No.,Date_from_clerk_website,Title,Votes_Total_Yes, Votes_Total_No,VOTE_DECISION,Alderman, Votes_Cleaned) %>% #select(Record.No., Ward, Date_from_clerk_website, Title, Vote_Total,Alderman, Votes_Cleaned) %>%
  spread(Alderman, Votes_Cleaned)


bill_table <- bill_table %>%
  mutate(Floor_Leader = if_else(Date_from_clerk_website >= "2019-05-28", "Waguespack", 
                                if_else(Date_from_clerk_website < "2019-05-28" &  
                                          Date_from_clerk_website >= "2019-01-10", "P. O’CONNOR", 
                                        if_else(Date_from_clerk_website < "2019-01-10", "Burke", "UNKNOWN")))) 
 
bill_table$Floor_Leader <- toupper(bill_table$Floor_Leader)


bill_table$Weekday <- weekdays(bill_table$Date_from_clerk_website) #add day of week
bill_table$Month <- format(bill_table$Date_from_clerk_website,"%B")
bill_table$Day <- format(bill_table$Date_from_clerk_website, "%d")
bill_table$Year <- format(bill_table$Date_from_clerk_website, "%Y")

####Create Alderman table -- rows are alderman and key variables are the bills with divided roll calls------------------------------------

alderman_table <- vb_df %>% 
  #filter(Date_from_clerk_website >= "2019-05-28") %>%
  filter(!is.na(Alderman), !is.na(Votes_Cleaned)) %>%
  #filter(Votes_Cleaned <= 1) %>%
  select(Record.No., Ward, Alderman, Votes_Cleaned) %>%
  spread(Record.No., Votes_Cleaned) 


agreements_df <- vector("character")
for(i in 1:nrow(alderman_table)) { #alderman_table$Alderman
  out.file <- vector("character")
  for(current_bill in bill_table$Record.No.) {
    
    Alderman = alderman_table[i, "Alderman"]
    floor_leader <- bill_table[which(bill_table$Record.No. == current_bill), "Floor_Leader"]
    mayor_proxy_vote <- alderman_table[which(alderman_table$Alderman == floor_leader), current_bill]
    ald_vote <- alderman_table[i, current_bill]
    if(!(ald_vote %in% c(1,0)) | !(mayor_proxy_vote %in% c(1,0)) | floor_leader == Alderman) {
          n_no_agreement_possible <- 1
          n_agreements <- 0
          n_disagreements <- 0
    
    } else if (mayor_proxy_vote == ald_vote) {
      
          n_no_agreement_possible <- 0
          n_agreements <- 1
          n_disagreements <- 0
      
    } else {
      
          n_no_agreement_possible <- 0
          n_agreements <- 0
          n_disagreements <- 1
      
      
    }

    in.file <- data.frame(n_no_agreement_possible, n_agreements, n_disagreements, stringsAsFactors = F)
    out.file <- bind_rows(out.file, in.file)
    
  }
  out.file <- out.file[-1,]
  ald_agreements_summary <- data.frame(Alderman, #= unique(na.omit(out.file$alderman))
                                       Ward = alderman_table[i, "Ward"],
                                       n_no_agreement_possible = sum(out.file$n_no_agreement_possible, na.rm = T),
                                       n_agreements = sum(out.file$n_agreements, na.rm = T),
                                       n_disagreements = sum(out.file$n_disagreements, na.rm = T), stringsAsFactors = F)
  
  agreements_df <- bind_rows(agreements_df, ald_agreements_summary)
  
}

agreements_df <- agreements_df[-1, ]

alderman_table <- merge(alderman_table, agreements_df, by = c("Alderman","Ward"), all.x = T)

alderman_table$n_possible_agreements <- alderman_table$n_agreements + alderman_table$n_disagreements
alderman_table$Percent_Agreement <- round(alderman_table$n_agreements / alderman_table$n_possible_agreements, 4)


alderman_table <- merge(alderman_table, alderman_stats %>% filter(!is.na(Ward)), by = c("Alderman","Ward"), all.x = T)




#Write tables -------------------------------------------------------------------------------------


#Graph friendly df
write.csv(vb_df, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_All.csv"), row.names = F)
write_feather(vb_df, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_All.feather"))


#Df for sharing and shiny
write.csv(shiny_df, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_All_SHARE.csv"), row.names = F)
write_feather(shiny_df, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_All_SHARE.feather"))
saveRDS(shiny_df, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_All_SHARE.rds"))


#Bills table
write.csv(bill_table, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_Summary_by_Bill.csv"), row.names = F)
write_feather(bill_table, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_Summary_by_Bill.feather"))


#Alderman table
write.csv(alderman_table, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_Summary_by_Alderman.csv"), row.names = F)
write_feather(alderman_table, here("tasks","clean","output","Chicago_Council_Divided_Votes_2019_Session_Summary_by_Alderman.feather"))


# Updated df for the Council Votes shiny app
rm(list=setdiff(ls(), c("vb_df", "shiny_df", "bill_table", "alderman_table")))
save.image(here("tasks","share","Council_Votes","Chicago_Council_Divided_Votes_2019_Session_all_objects.RData"))

 




